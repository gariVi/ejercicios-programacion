/*

Programa: Ejercicio 8_Tablas de multiplicar - (opcional)
Objetivo: Elabora un programa que reciba un n�mero e imprima la tabla de multiplicar de ese n�mero del 2 al 10.
Curso: Introducci�n a la Programaci�n.
Elaborado por: Garc�a Rangel Isis Violeta

*/

#include <stdio.h>

int main(){
	
	int i, numero;
	char repetir;
	
	printf("--- Programa que te muestra una tabla de multiplicar ---\n");
    do{
        printf("\nIntroduce un n%cmero: ", 163);
        scanf("%d", &numero);
	    for( i = 2 ; i <= 10 ; i++ ){
	        printf("\n%d * %d = %d\n", i, numero, i * numero);
	    }
	    
        printf("\n%cDeseas realizar otra tabla de multiplicar? (s/n): ", 168);
        fflush( stdin );
        scanf("%c", &repetir);
        
    }while (repetir != 'n');
    
    return 0;
}
