/*

Programa: Ejercicio 5_Tri�ngulo Escaleno
Objetivo: Elabora un programa que calcule el per�metro de un tri�ngulo escaleno. El programa pedir� al usuario las entradas necesarias.
Curso: Introducci�n a la Programaci�n.
Elaborado por: Garc�a Rangel Isis Violeta

*/

#include <stdio.h>

int main()
{
	
	float ladoUno, ladoDos, ladoTres, perimEscaleno;
	
	printf("--- Programa que calcula el per%cmetro de un Tri%cngulo Escaleno --- \n", 161, 160);
	
	printf("\nRecuerda que en un tri%Cngulo escaleno todos los lados son diferentes.\n", 160);
	
	printf("\nIntroduce la medida del lado uno: ");
	
	scanf("%f", &ladoUno); //Leemos el valor de ladoUno
	
	printf("\nIntroduce la medida del lado dos: ");
	
	scanf("%f", &ladoDos); //Leemos el valor de ladoDos
	
	printf("\nIntroduce la medida del lado tres: ");
	
	scanf("%f", &ladoTres); //Leemos el valor de ladoTres
	
	if(ladoUno >= 1 && ladoDos >= 1 && ladoTres >= 1){ //Las tres medidas deben de ser iguales o mayores a 1
		
		perimEscaleno= ladoUno + ladoDos + ladoTres;
		printf("\nEl per%cmetro de tu tri%cngulo es: %.2f", 161, 160, perimEscaleno);
		
		
	}else{
		
		printf("\nTodas las medidas deben de ser positivas para calcular el per%cmetro.", 161); //Ninguna de las medidas pueden ser negativas.
		
	}
	
	return 0;
	
}


