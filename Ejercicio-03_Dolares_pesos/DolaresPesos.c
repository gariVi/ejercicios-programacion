/*

Programa: Ejercicio 3_D�lares a pesos
Objetivo: Elabora un programa que reciba como entrada un monto en d�lares (USD) y devuelva la cantidad equivalente en pesos mexicanos (MXN).
Curso: Introducci�n a la Programaci�n.
Elaborado por: Garc�a Rangel Isis Violeta

*/

#include <stdio.h>

int main()
{
	
	float valorDolar, numDolar, converDolar;
	
	valorDolar =  22.0033; //Le asigne este valor a los d�lares porque es el m�s reciente del 16 de Agosto del presente a�o y no lo puse como constante porque se entiende que es un valor que esta en constante cambio.
	
	printf("--- Programa que da el equivalente de D%clares a Pesos --- \n", 162);
	
	printf("\nIntroduce tu cantidad de d%clares (USD): ", 162);
	
	scanf("%f", &numDolar); //Leemos la cantidad de d�lares
	
	if(numDolar >= 1){ //Se aceptan cantidades desde 1 d�lar
		
		converDolar = ((valorDolar) * (numDolar));
		printf("\n Tu equivalente en pesos mexicanos es: %.2f (MXN)", converDolar);
		
		
	}else{
		
		printf("\nNo se aceptan cantidades negativas."); //No se aceptan canidades negativas de d�lares
		
	}
	
	return 0;
	
}


