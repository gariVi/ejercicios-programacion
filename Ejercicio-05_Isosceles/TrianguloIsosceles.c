/*

Programa: Ejercicio 5_Tri�ngulo Is�sceles
Objetivo: Elabora un programa que calcule el per�metro de un tri�ngulo is�sceles. El programa pedir� al usuario las entradas necesarias.
Curso: Introducci�n a la Programaci�n.
Elaborado por: Garc�a Rangel Isis Violeta

*/

#include <stdio.h>

int main()
{
	
	float ladoUno, ladoDos, perimIsosceles;
	
	printf("--- Programa que calcula el per%cmetro de un Tri%cngulo Is%csceles --- \n", 161, 160, 162);
	
	printf("\nRecuerda que en un tri%Cngulo is%csceles son dos lados iguales y uno diferente.\n", 160, 162);
	
	printf("\nIntroduce la medida que abarca dos lados del  tri%cngulo: ", 160);
	
	scanf("%f", &ladoUno); //Leemos el valor de ladoUno
	
	printf("\nIntroduce la medida diferente de un lado del tri%cngulo: ", 160);
	
	scanf("%f", &ladoDos); //Leemos el valor de ladoDos
	
	if(ladoUno >= 1 && ladoDos >= 1){ //Ambas medidas deben de ser iguales o mayores a 1
		
		perimIsosceles = ((2 * ladoUno) + (ladoDos));
		printf("\nEl per%cmetro de tu tri%cngulo es: %.2f", 161, 160, perimIsosceles);
		
		
	}else{
		
		printf("\nAmbas medidas deben de ser positivas para calcular el per%cmetro.", 161); //Ninguna de las dos medidas pueden ser negativas.
		
	}
	
	return 0;
	
}


