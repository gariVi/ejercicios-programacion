/*

Programa: Ejercicio 2_Suma de consecutivos
Objetivo: Elabora un programa que reciba un n�mero entre 1 y 50 y devuelva la suma de los n�meros consecutivos del 1 hasta ese n�mero.
Curso: Introducci�n a la Programaci�n.
Elaborado por: Garc�a Rangel Isis Violeta

*/

#include <stdio.h>

int main()
{
	
	float limite, sumaConse;
	
	printf("--- Programa que te da la Suma de Consecutivos ---\n");
	
	printf("\nRecuerda que puedes escoger un n%cmero del 1 al 50 para realizar la suma.", 163);
	
	printf("\nIngresa el l%cmite de tu suma: ", 161);
	
	scanf("%f", &limite); //Leemos el l�mite para la suma
	
	if(limite >= 1 && limite <= 50){ //El l�mite tiene que estar entre el 1 y 50
		
		sumaConse = (((1 + limite) / (2)) * (limite));
		printf("\n La suma es: %.2f", sumaConse);
		
		
	}else{
		
		printf("\nEl l%cmite ingresado no es v%clido.", 161, 160); //Si es un valor mayor a 50 o menor a 1, o un negativo no se puede hacer la suma
		
	}
	
	return 0;
	
}

