/*

Programa: Ejercicio 4_Tri�ngulo equil�tero
Objetivo: Elabora un programa calcule el per�metro de un tri�ngulo equil�tero. El programa pedir� al usuario las entradas necesarias.
Curso: Introducci�n a la Programaci�n.
Elaborado por: Garc�a Rangel Isis Violeta

*/

#include <stdio.h>

int main()
{
	
	float lado, perimEquilatero;
	
	printf("--- Programa que calcula el per%cmetro de un Tri%cngulo Equil%ctero --- \n", 161, 160, 160);
	
	printf("\nRecuerda que en un tri%cngulo equil%ctero todos los lados son iguales.\n", 160, 160);
	
	printf("\nIntroduce cuanto mide el lado de tu tri%cngulo: ", 160);
	
	scanf("%f", &lado); //Leemos el valor de lado
	
	if(lado >= 1){ //La medida debe ser igual o mayor a 1 de la unidades en que se mida el per�metro
		
		perimEquilatero = ((lado) * (3));
		printf("\nEl per%cmetro de tu tri%cngulo es: %.2f", 161, 160, perimEquilatero);
		
		
	}else{
		
		printf("\nNo se aceptan medidas negativas."); //No hay medidas negativas porque no es posible per�metros negativos.
		
	}
	
	return 0;
	
}


