/*

Programa: Ejercicio 7_Per�metro del tri�ngulo - (opcional)
Objetivo: Elabora programa que calcule el per�metro de un tri�ngulo. El programa preguntar� al usuario el tipo de tri�ngulo (equil�tero, is�sceles o escaleno) y le pedir� las entradas necesarias para realizar el c�lculo necesario.
Curso: Introducci�n a la Programaci�n.
Elaborado por: Garc�a Rangel Isis Violeta

*/

#include <stdio.h>

int main()
{
	char repetir;
	int opTriangulo;
	float lado, perimEquilatero;
	float ladoUnoIso, ladoDosIso, perimIsosceles;
	float ladoUnoEs, ladoDosEs, ladoTresEs, perimEscaleno;
	
	float perimetroEquilatero(float lado, float perimEquilatero){
		
			printf("\nRecuerda que en un tri%cngulo equil%ctero todos los lados son iguales.\n", 160, 160);
	
			printf("\nIntroduce cuanto mide el lado de tu tri%cngulo: ", 160);
	
			scanf("%f", &lado); //Leemos el valor de lado
	
			if(lado >= 1){ //La medida debe ser igual o mayor a 1 de la unidades en que se mida el per�metro
		
				perimEquilatero = ((lado) * (3));
				printf("\nEl per%cmetro de tu tri%cngulo es: %.2f", 161, 160, perimEquilatero);
		
			}else{
		
				printf("\nNo se aceptan medidas negativas."); //No hay medidas negativas porque no es posible per�metros negativos.
		
			}
			
	}
	
	float perimetroIsosceles(float ladoUnoIso, float ladoDosIso, float perimIsosceles){
		
			printf("\nRecuerda que en un tri%Cngulo is%csceles son dos lados iguales y uno diferente.\n", 160, 162);
	
			printf("\nIntroduce la medida que abarca dos lados del  tri%cngulo: ", 160);
	
			scanf("%f", &ladoUnoIso); //Leemos el valor de ladoUno
	
			printf("\nIntroduce la medida diferente de un lado del tri%cngulo: ", 160);
	
			scanf("%f", &ladoDosIso); //Leemos el valor de ladoDos
	
			if(ladoUnoIso >= 1 && ladoDosIso >= 1){ //Ambas medidas deben de ser iguales o mayores a 1
		
				perimIsosceles = ((2 * ladoUnoIso) + (ladoDosIso));
				printf("\nEl per%cmetro de tu tri%cngulo es: %.2f", 161, 160, perimIsosceles);
		
			}else{
		
				printf("\nAmbas medidas deben de ser positivas para calcular el per%cmetro.", 161); //Ninguna de las dos medidas pueden ser negativas.
		
			}
		
	}
	
	float perimetroEscaleno(float ladoUnoEs, float ladoDosEs, float ladoTresEs, float perimEscaleno){
		
			printf("\nRecuerda que en un tri%Cngulo escaleno todos los lados son diferentes.\n", 160);
	
			printf("\nIntroduce la medida del lado uno: ");
	
			scanf("%f", &ladoUnoEs); //Leemos el valor de ladoUno
	
			printf("\nIntroduce la medida del lado dos: ");
	
			scanf("%f", &ladoDosEs); //Leemos el valor de ladoDos
	
			printf("\nIntroduce la medida del lado tres: ");
	
			scanf("%f", &ladoTresEs); //Leemos el valor de ladoTres
	
			if(ladoUnoEs >= 1 && ladoDosEs >= 1 && ladoTresEs >= 1){ //Las tres medidas deben de ser iguales o mayores a 1
		
				perimEscaleno= ladoUnoEs + ladoDosEs + ladoTresEs;
				printf("\nEl per%cmetro de tu tri%cngulo es: %.2f", 161, 160, perimEscaleno);
		
			}else{
		
				printf("\nTodas las medidas deben de ser positivas para calcular el per%cmetro.", 161); //Ninguna de las medidas pueden ser negativas.
		
			}
		
	}
	
	printf("--- Programa que calcula el per%cmetro de un Tri%cngulo --- \n", 161, 160);
	
	do{
	
	printf("\nIntroduce el n%cmero que le corresponda al tipo de tri%cngulo que deseas calcular.\n", 163,160);
	
	printf("\n1.- Tri%cngulo Equil%ctero\n", 160, 160);
	
	printf("\n2.- Tri%cngulo Is%csceles\n", 160, 162);
	
	printf("\n3.- Tri%cngulo Escaleno\n", 160);
	
	printf("\nOpci%cn: ", 162);

	scanf("%d", &opTriangulo);
	
	switch(opTriangulo)
	{
		
		case 1:
			
			perimetroEquilatero(lado, perimEquilatero);
			
			break;
			
		case 2:
			
			perimetroIsosceles(ladoUnoIso, ladoDosIso, perimIsosceles);
			
			break;
			
		case 3:
			
			perimetroEscaleno(ladoUnoEs, ladoDosEs, ladoTresEs, perimEscaleno);
			
			break;
			
		default:
			printf("La opci%cn escogida no es v%clida, debe de ser del 1-3", 162, 160);
		
	}
	
	printf("\n\n%cDeseas calcular otro per%cmetro?(s/n): ", 168, 161);
    fflush( stdin );
    scanf( "%c", &repetir );
	
	} while (repetir != 'n');
	
	return 0;
	
}



