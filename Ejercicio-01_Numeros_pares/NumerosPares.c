/*

Programa: Ejercicio 1_N�meros pares
Objetivo: Elabora un programa que imprima los n�meros pares del 0 al 100.
Curso: Introducci�n a la Programaci�n.
Elaborado por: Garc�a Rangel Isis Violeta

*/

#include <stdio.h>

int main()
{
	
	printf("--- Programa que da los n%cmeros pares del 0 al 100 ---\n", 163);
	
	printf("\nLos n%cmeros pares son:\n", 163);
	
	int cont = 0;
	
	while(cont <= 100) //Condici�n While, donde el contador tiene que ser menor o igual a 100 que es nuestro l�mite
	{
		if(cont % 2 == 0) //Condici�n If, para validar con la operaci�n MOD que n�meros divididos entre 2 dan como residuo 0
		{
			printf("%i\n", cont); //Imprimimos el contador
			
		}
		cont++; //Incrementamos el contador para que consiga los n�meros pares hasta nuestro l�mite que es 100
	}
	
	return 0;
}
