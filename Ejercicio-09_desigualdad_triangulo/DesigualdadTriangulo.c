/*

Programa: Ejercicio 9_ Desigualdad del tri�ngulo - (opcional)
Objetivo: Elabora un programa que reciba la longitud de 3 segmentos y determine si con ellos es posible formar un tri�ngulo.
Curso: Introducci�n a la Programaci�n.
Elaborado por: Garc�a Rangel Isis Violeta

*/

#include <stdio.h>

int main(){
	
	float medidaA, medidaB, medidaC;
	char repetir;
	
	printf("--- Programa que te indica si es posible formar un tri%cngulo ---\n", 160);
    do{
        printf("\nIntroduce la medida del lado A: ");
        scanf("%f", &medidaA);
        printf("\nIntroduce la medida del lado B: ");
        scanf("%f", &medidaB);
        printf("\nIntroduce la medida del lado C: ");
        scanf("%f", &medidaC);
        
	    if(medidaA+medidaB > medidaC && medidaB+medidaC > medidaA && medidaA+medidaC >medidaB){
	    	
	    	printf("\nSi es posible formar el tri%cngulo con estas medidas.\n", 160);
	    	
		}else{
			
			printf("\nNo es posible formar el tri%cngulo con estas medidas, seg%cn el teorema de la desigualdad del tri%cngulo.\n", 160, 163, 160);
			
		}
	    
        printf("\n%cDeseas checar con otras medidas? (s/n): ", 168);
        fflush( stdin );
        scanf("%c", &repetir);
        
    }while (repetir != 'n');
    
    return 0;
}
